​def get_user_feature_layer(uid_embed_layer,gender_embed_layer,age_embed_layer,job_embed_layer):
    with tf.name_scope("user_fc"):
        uid_fc_layer = tf.layers.dense(uid_embed_layer, embed_dim,name = "uid_fc_layer", activation= tf.nn.relu)
        gender_fc_layer = tf.layers.dense(gender_embed_layer, embed_dim, name = "gender_fc_layer",activation = tf.nn.relu)
        age_fc_layer = tf.layers.dense(age_embed_layer,embed_dim, name = "age_fc_layer",activation = tf.nn.relu)
        job_fc_layer = tf.layers.dense(job_embed_layer,embed_dim,name="job_fc_layer",activation = tf.nn.relu)
        
        user_combine_layer = tf.concat([uid_fc_layer,gender_fc_layer,age_fc_layer,job_fc_layer],2)
        user_combine_layer = tf.contrib.layers.fully_connected(user_combine_layer,200,tf.tann)
        
        user_combine_layer_flat = tf.reshape(user_combine_layer,[-1,200])
    return user_combine_layer,user_combine_layer_flat
        
 