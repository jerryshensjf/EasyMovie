import matplotlib.pyplot as plt
import pickle
import pdb

losses = pickle.load(open('losses.p', mode='rb'))

plt.plot(losses['train'], label='Training loss')
plt.legend()
_ = plt.ylim()

plt.show()