import matplotlib.pyplot as plt
import pickle
import pdb

movie_matrics = pickle.load(open('movie_matrics.p', mode='rb'))
print(movie_matrics)

plt.plot(movie_matrics, label='Training loss')
plt.legend()
_ = plt.ylim()

plt.show()