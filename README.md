#  EasyMovie电影TensorFlow推荐系统

### 项目介绍
EasyMovie是一套使用TensorFlow的电影推荐系统，基于常用的电影数据集（ml-1m)。
本软件基于流行的TensorFlow例程，但做了自己的演绎。使用Python语言。
本软件并非原创，而是有自己的原作者，也是开源软件。如您发现原作的地址，请提供给我，我会即时更新。

最近，将TensorFlow的版本迁徙至TensorFlow2.16.1。并增加了独立的结果读取程序。

作者：沈戟峰 jerry_shen_sjf@qq.com

### 软件架构

软件架构说明

- Python 3.11
- TensorFlow 2.16.1


### 使用说明

1. 使用pip安装依赖包
1. 使用 python MovieRecommand2.py训练模型。
1. 使用 python LookupResult.py you 20 10读取20号用户前10条你喜欢的电影。
1. 使用 python LookupResult.py same 20 10读取20号电影前10条同样类型的电影。
1. 使用 python LookupResult.py other 20 10读取20号电影前10条其他用户喜欢的电影。

### 交流QQ群


- 一群：动词算子式代码生成器群 277689737
- 二群：UI设计实验室 70646187
- 三群：动力建站 255973110


 